import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

# Create dataframe
df = pd.read_csv("hr_employee_data.csv")
# print("Df shape:", df.shape)
# print("Df info:", df.info())

# Check how many people left the job
sns.countplot(x="left", data=df)
plt.show()

# Check salary distribution
sns.countplot(x="salary", data=df)
plt.show()

# Check how many people left, according to their salary
sns.catplot(x="left", col="salary", kind="count", data=df)
plt.show()

# Check satisfaction levels
sns.distplot(df["satisfaction_level"])
plt.show()

# Check how many people left, according to their satisfaction level
# Now using a boxplot
sns.boxplot(x="left", y="satisfaction_level", data=df)
plt.show()

# Check correlation between all variables
cor_mat = df.corr()
fig = plt.figure(figsize=(15,7))
sns.heatmap(cor_mat, annot=True)
plt.show()