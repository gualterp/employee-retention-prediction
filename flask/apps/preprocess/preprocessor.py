import pandas as pd
import numpy as np
import json
from sklearn.impute import KNNImputer
from apps.core.logger import Logger


class Preprocessor:

    def __init__(self, run_id, data_path, mode):
        self.run_id = run_id
        self.data_path = data_path
        self.logger = Logger(self.run_id, 'Preprocessor', mode)

    def get_data(self):
        try:
            # Reading the data file
            self.logger.info('Start reading dataset...')
            self.data = pd.read_csv(self.data_path + '_validation/InputFile.csv')
            self.logger.info('End reading dataset.')
            return self.data
        except Exception as e:
            self.logger.exception('Exception raised while reading dataset: %s' + str(e))
            raise Exception()

    def drop_columns(self, data, columns):
        self.data = data
        self.columns = columns
        try:
            self.logger.info('Start droping columns.')
            self.useful_data = self.data.drop(labels=self.columns, axis=1)
            self.logger.info('End droping columns.')
            return self.useful_data
        except Exception as e:
            self.logger.exception('Exception raised while droping columns:' + str(e))
            raise Exception()

    def is_null_present(self, data):
        self.null_present = False
        try:
            self.logger.info('Start finding missing values...')
            self.null_counts = data.isna().sum()  # Check for the count of null values per column
            for i in self.null_counts:
                if i > 0:
                    self.null_present = True
                    break
            if (self.null_present):  # Write the logs to see which columns have null values
                dataframe_with_null = pd.DataFrame()
                dataframe_with_null['columns'] = data.columns
                dataframe_with_null['missing values count'] = np.asarray(data.isna().sum())
                dataframe_with_null.to_csv(
                    self.data_path + '_validation/' + 'null_values.csv')
            self.logger.info('End finding missing values.')
            return self.null_present
        except Exception as e:
            self.logger.exception('Exception raised while finding missing values:' + str(e))
            raise Exception()

    def impute_missing_values(self, data):
        self.data = data
        try:
            self.logger.info('Start imputing missing values...')
            imputer = KNNImputer(n_neighbors=3, weights='uniform', missing_values=np.nan)
            self.new_array = imputer.fit_transform(self.data)  # Impute the missing values
            # Convert the nd-array returned in the step above to a Data frame
            self.new_data = pd.DataFrame(data=self.new_array, columns=self.data.columns)
            self.logger.info('End imputing missing values.')
            return self.new_data
        except Exception as e:
            self.logger.exception('Exception raised while imputing missing values:' + str(e))
            raise Exception()

    def feature_encoding(self, data):
        try:
            self.logger.info('Start feature encoding...')
            self.new_data = data.select_dtypes(include=['object']).copy()
            # Using the dummy encoding to encode the categorical columns to numerical ones
            for col in self.new_data.columns:
                self.new_data = pd.get_dummies(self.new_data, columns=[col], prefix=[col], drop_first=True)
            self.logger.info('End feature encoding.')
            return self.new_data
        except Exception as e:
            self.logger.exception('Exception raised while feature encoding:' + str(e))
            raise Exception()

    def split_features_label(self, data, label_name):
        self.data = data
        try:
            self.logger.info('Start splitting features and label...')
            self.X = self.data.drop(labels=label_name,
                                    axis=1)  # Drop the columns specified and separate the feature columns
            self.y = self.data[label_name]  # Filter the Label columns
            self.logger.info('End splitting features and label.')
            return self.X, self.y
        except Exception as e:
            self.logger.exception('Exception raised while splitting features and label:' + str(e))
            raise Exception()

    def final_predictset(self, data):
        try:
            self.logger.info('Start building final predict set...')
            with open('apps/database/columns.json', 'r') as f:
                data_columns = json.load(f)['data_columns']
                f.close()
            df = pd.DataFrame(data=None, columns=data_columns)
            df_new = pd.concat([df, data], ignore_index=True, sort=False)
            data_new = df_new.fillna(0)
            self.logger.info('End building final predict set.')
            return data_new
        except ValueError:
            self.logger.exception('ValueError raised while building final predict set')
            raise ValueError
        except KeyError:
            self.logger.exception('KeyError raised while building final predict set')
            raise KeyError
        except Exception as e:
            self.logger.exception('Exception raised while building final predict set: %s' % e)
            raise e

    def preprocess_trainset(self):
        try:
            self.logger.info('Start preprocessing...')
            # Get data into pandas data frame
            data = self.get_data()
            # Drop unwanted columns
            data = self.drop_columns(data, ['empid'])
            # Jandle label encoding
            cat_df = self.feature_encoding(data)
            data = pd.concat([data, cat_df], axis=1)
            # Drop categorical column
            data = self.drop_columns(data, ['salary'])
            # Check if missing values are present in the data set
            is_null_present = self.is_null_present(data)
            # If missing values are there, replace them appropriately.
            if (is_null_present):
                data = self.impute_missing_values(data)  # Missing value imputation
            # Create separate features and label
            self.X, self.y = self.split_features_label(data, label_name='left')
            self.logger.info('End preprocessing.')
            return self.X, self.y
        except Exception:
            self.logger.exception('Unsuccessful end of preprocessing.')
            raise Exception

    def preprocess_predictset(self):
        try:
            self.logger.info('Start of preprocessing...')
            # Get data into pandas data frame
            data = self.get_data()
            # Handle label encoding
            cat_df = self.feature_encoding(data)
            data = pd.concat([data, cat_df], axis=1)
            # Drop categorical column
            data = self.drop_columns(data, ['salary'])
            # Check if missing values are present in the data set
            is_null_present = self.is_null_present(data)
            # If missing values are there, replace them appropriately.
            if (is_null_present):
                data = self.impute_missing_values(data)
            data = self.final_predictset(data)
            self.logger.info('End of preprocessing.')
            return data
        except Exception:
            self.logger.exception('Unsuccessful end of preprocessing.')
            raise Exception

    def preprocess_predict(self, data):
        try:
            self.logger.info('Start preprocessing...')
            cat_df = self.feature_encoding(data)
            data = pd.concat([data, cat_df], axis=1)
            # Drop categorical column
            data = self.drop_columns(data, ['salary'])
            # Check if missing values are present in the data set
            is_null_present = self.is_null_present(data)
            # If missing values are there, replace them appropriately.
            if (is_null_present):
                data = self.impute_missing_values(data)
            data = self.final_predictset(data)
            self.logger.info('End of preprocessing.')
            return data
        except Exception:
            self.logger.exception('Unsuccessful End of Preprocessing.')
            raise Exception
