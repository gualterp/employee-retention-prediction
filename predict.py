import pandas as pd

from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from xgboost import XGBClassifier
from sklearn.model_selection import GridSearchCV

# Create dataframe
pre_df = pd.read_csv("hr_employee_data.csv")

### Feature engineering ###

# The employee ID, for example, won't add any value to the prediction
df = pre_df.copy()
df.drop(['empid'], axis=1, inplace=True)

# Check for missing values
# print(df.isnull().sum()) -- some detected in variable satisfaction_level
# print(df['satisfaction_level'].describe()) 

# Fill missing values with the mean value
df['satisfaction_level'].fillna(df['satisfaction_level'].mean(), inplace=True)

# Map salary variable, which takes string values, to integer values
# print(df['salary'].unique())
df = df.replace({"low" : 1, "medium" : 2, "high" : 3})

### Split dataset ###

# Training and test
X = df.drop(labels="left", axis=1)
y = df['left']
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2)

### Model selection ###
# Will be using two classifiers, Random Forest and XGBoost

# Define parameters for model testing
model_params = {
    "RandomForestClassifier": {
        "model" : RandomForestClassifier(),
        "param" : {
            "n_estimators" : [10, 50, 100, 130],
            "criterion" : ["gini", "entropy"],
            "max_depth" : range(2,4,1),
            "max_features" : ["auto", "log2"]            
        }        
    },
    "XGBClassifier" : {
        "model" : XGBClassifier(objective="binary:logistic"),
        "param" : {
            "learning_rate" : [0.5, 0.1, 0.01, 0.001],
            "max_depth" : [3, 5, 10, 20],
            "n_estimators" : [10, 50, 100, 200],
            "eval_metric" : ["logloss"]
        }        
    }    
}

# Hyperparameter optimization
scores = []
for model_name, mp in model_params.items() :
    model_selection = GridSearchCV(estimator=mp["model"], 
                                   param_grid=mp["param"],
                                   cv=5, return_train_score=False)
    model_selection.fit(X, y)
    scores.append({
        "model" : model_name,
        "best_score" : model_selection.best_score_,
        "best_params" : model_selection.best_params_        
    })
    
# Build model
# print(scores)

xgb = XGBClassifier(objective="binary:logistic", learning_rate=0.1,
                    max_depth=20, n_estimators=200, eval_metric="logloss")
xgb.fit(X_train, y_train)
print("Score:", xgb.score(X_test, y_test))
